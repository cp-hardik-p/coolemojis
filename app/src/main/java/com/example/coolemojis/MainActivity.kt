package com.example.coolemojis


import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.animation.core.*
import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Path
import androidx.compose.ui.unit.dp
import com.example.coolemojis.ui.theme.CoolEmojisTheme
import com.example.coolemojis.ui.theme.EmojiEyeColor
import com.example.coolemojis.ui.theme.ExclamationEmojiColor
import com.example.coolemojis.ui.theme.TongueColor

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            CoolEmojisTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = Color.Blue.copy(0.1f)
                ) {
                    CoolEmojis()
                }
            }
        }
    }
}

@Composable
fun CoolEmojis() {
    val scrollState = rememberScrollState()
    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Top,
        modifier = Modifier.verticalScroll(scrollState)
    ) {
        val infiniteTransition = rememberInfiniteTransition()
        ConfusedEmoji()
        HappyEmoji()
        CoolEmoji()
        ExclamationEmoji(infiniteTransition)
        LongTongueEmoji()
        SadEmoji()
    }
}

@Composable
fun animateValues(
    values: List<Float>,
    animationSpec: AnimationSpec<Float> = spring(),
): State<Float> {

    // 1. Create the groups zipping with next entry
    val groups by rememberUpdatedState(newValue = values.zipWithNext())
    // 2. Start the state with the first value
    val state = remember { mutableStateOf(values.first()) }

    LaunchedEffect(key1 = groups) {
        val (_, setValue) = state
        // Start the animation from 0 to groups quantity
        animate(
            initialValue = 0f,
            targetValue = groups.size.toFloat(),
            animationSpec = animationSpec,
        ) { frame, _ ->
            // Get which group is being evaluated
            val integerPart = frame.toInt()
            val (initialValue, finalValue) = groups[frame.toInt()]
            // Get the current "position" from the group animation
            val decimalPart = frame - integerPart
            // Calculate the progress between the initial and final value
            setValue(
                initialValue + (finalValue - initialValue) * decimalPart
            )
        }
    }
    return state
}

@Composable
fun ConfusedEmoji() {
    val easing = LinearOutSlowInEasing
    val offsetX by animateValues(
        values = listOf(0f, 15f, -15f, 0f),
        animationSpec = infiniteRepeatable(
            animation = tween(durationMillis = 1500, easing = easing),
            repeatMode = RepeatMode.Restart
        )
    )

    Canvas(
        modifier = Modifier
            .padding(top = 16.dp)
            .size(100.dp)
    ) {
        drawArc(
            color = Color(0xFFFFd301),
            startAngle = 0f,
            sweepAngle = 360f,
            useCenter = true
        )

        drawCircle(
            color = Color.White,
            center = Offset(x = 35.dp.toPx(), y = 30.dp.toPx()),
            radius = 30f,
        )

        drawCircle(
            color = Color.Black,
            center = Offset(-offsetX + 35.dp.toPx(), 30.dp.toPx()),
            radius = 10f,
        )

        drawCircle(
            color = Color.White,
            center = Offset(x = 65.dp.toPx(), y = 30.dp.toPx()),
            radius = 30f,
        )

        drawCircle(
            color = Color.Black,
            center = Offset(-offsetX + 65.dp.toPx(), 30.dp.toPx()),
            radius = 10f,
        )

        drawLine(
            start = Offset(x = 30.dp.toPx(), y = 70.dp.toPx()),
            end = Offset(x = 70.dp.toPx(), y = 70.dp.toPx()),
            color = Color.Black,
            strokeWidth = 10.0f
        )
    }
}

@Composable
fun HappyEmoji() {
    val size = 100
    Canvas(
        modifier = Modifier
            .padding(top = 16.dp)
            .size(size.dp)
    ) {

        drawArc(
            color = Color(0xFFFFd301),
            startAngle = 0f,
            sweepAngle = 360f,
            useCenter = true
        )

        drawCircle(
            color = Color.White,
            center = Offset(x = 35.dp.toPx(), y = 30.dp.toPx()),
            radius = 30f,
        )

        drawCircle(
            color = Color.Black,
            center = Offset(x = 35.dp.toPx(), y = 30.dp.toPx()),
            radius = 10f,
        )

        drawCircle(
            color = Color.White,
            center = Offset(x = 65.dp.toPx(), y = 30.dp.toPx()),
            radius = 30f,
        )

        drawCircle(
            color = Color.Black,
            center = Offset(x = 65.dp.toPx(), y = 30.dp.toPx()),
            radius = 10f,
        )

        val mouthPath = Path().let {
            it.moveTo(size * 0.22.dp.toPx(), size * 0.70.dp.toPx())
            it.quadraticBezierTo(
                size * 0.50.dp.toPx(), size * 0.80.dp.toPx(),
                size * 0.78.dp.toPx(), size * 0.70.dp.toPx()
            )
            it.quadraticBezierTo(
                size * 0.50.dp.toPx(), size * 0.95.dp.toPx(),
                size * 0.22.dp.toPx(), size * 0.70.dp.toPx()
            )
            it.close()
            it
        }
        drawPath(path = mouthPath, color = Color.Black)
    }
}

@Composable
fun CoolEmoji() {
    val size = 100
    Canvas(
        modifier = Modifier
            .padding(top = 16.dp)
            .size(size.dp)
    ) {
        drawArc(
            color = Color(0xFFFFd301),
            startAngle = 0f,
            sweepAngle = 360f,
            useCenter = true
        )

        drawCircle(
            color = Color.White,
            center = Offset(x = 35.dp.toPx(), y = 30.dp.toPx()),
            radius = 30f,
        )

        drawCircle(
            color = Color.Black,
            center = Offset(x = 35.dp.toPx(), y = 30.dp.toPx()),
            radius = 10f,
        )

        drawCircle(
            color = Color.White,
            center = Offset(x = 65.dp.toPx(), y = 30.dp.toPx()),
            radius = 20f,
        )

        drawCircle(
            color = Color.Black,
            center = Offset(x = 65.dp.toPx(), y = 30.dp.toPx()),
            radius = 10f,
        )

        val mouthPath = Path().let {
            it.moveTo(size * 0.20.dp.toPx(), size * 0.70.dp.toPx())
            it.quadraticBezierTo(
                size * 0.50.dp.toPx(), size * 0.80.dp.toPx(),
                size * 0.78.dp.toPx(), size * 0.70.dp.toPx()
            )
            it.quadraticBezierTo(
                size * 0.50.dp.toPx(), size * 1.60.dp.toPx(),
                size * 0.20.dp.toPx(), size * 0.70.dp.toPx()
            )
            it.close()
            it
        }
        drawPath(path = mouthPath, color = TongueColor)
    }
}

@Composable
fun ExclamationEmoji(infiniteTransition: InfiniteTransition) {
    val eyeAnimation by infiniteTransition.animateFloat(
        initialValue = 1f,
        targetValue = 30f,
        animationSpec = infiniteRepeatable(
            animation = tween(1500, easing = FastOutSlowInEasing),
            repeatMode = RepeatMode.Reverse
        )
    )

    Canvas(
        modifier = Modifier
            .padding(top = 40.dp)
            .size(100.dp)
    ) {
        drawArc(
            color = Color(0xFFFFd301),
            startAngle = 0f,
            sweepAngle = 360f,
            useCenter = true
        )
        drawCircle(
            color = Color.White,
            center = Offset(x = 35.dp.toPx(), y = 30.dp.toPx()),
            radius = eyeAnimation,
        )

        drawCircle(
            color = Color.Black,
            center = Offset(x = 35.dp.toPx(), y = 30.dp.toPx()),
            radius = 10f,
        )

        drawCircle(
            color = Color.White,
            center = Offset(x = 65.dp.toPx(), y = 30.dp.toPx()),
            radius = eyeAnimation,
        )

        drawCircle(
            color = Color.Black,
            center = Offset(x = 65.dp.toPx(), y = 30.dp.toPx()),
            radius = 10f,
        )

        drawCircle(
            color = ExclamationEmojiColor,
            center = Offset(x = 50.dp.toPx(), y = 70.dp.toPx()),
            radius = 20f,
        )
    }
}

@Composable
fun LongTongueEmoji() {
    val size = 100
    Canvas(
        modifier = Modifier
            .padding(top = 16.dp)
            .size(size.dp)
    ) {

        drawArc(
            color = Color(0xFFFFd301),
            startAngle = 0f,
            sweepAngle = 360f,
            useCenter = true
        )

        drawOval(
            color = EmojiEyeColor,
            topLeft = Offset(x = 25.dp.toPx(), y = 30.dp.toPx()),
            size = Size(20f, 35f)
        )

        drawOval(
            color = EmojiEyeColor,
            topLeft = Offset(x = 65.dp.toPx(), y = 30.dp.toPx()),
            size = Size(20f, 35f)
        )

        val mouthPath = Path().let {
            it.moveTo(size * 0.20.dp.toPx(), size * 0.70.dp.toPx())
            it.quadraticBezierTo(
                size * 0.50.dp.toPx(), size * 0.80.dp.toPx(),
                size * 0.78.dp.toPx(), size * 0.70.dp.toPx()
            )
            it.quadraticBezierTo(
                size * 0.50.dp.toPx(), size * 1.90.dp.toPx(),
                size * 0.20.dp.toPx(), size * 0.70.dp.toPx()
            )
            it.close()
            it
        }
        drawPath(path = mouthPath, color = TongueColor)
    }
}

@Composable
fun SadEmoji() {
    val size = 100
    Canvas(
        modifier = Modifier
            .padding(top = 40.dp)
            .size(size.dp)
    ) {

        drawArc(
            color = Color(0xFFFFd301),
            startAngle = 0f,
            sweepAngle = 360f,
            useCenter = true
        )

        drawOval(
            color = EmojiEyeColor,
            topLeft = Offset(x = 25.dp.toPx(), y = 30.dp.toPx()),
            size = Size(20f, 35f)
        )

        drawOval(
            color = EmojiEyeColor,
            topLeft = Offset(x = 65.dp.toPx(), y = 30.dp.toPx()),
            size = Size(20f, 35f)
        )

        val mouthPath = Path().let {
            it.moveTo(size * 0.30.dp.toPx(), size * 0.70.dp.toPx())
            it.quadraticBezierTo(
                size * 0.45.dp.toPx(), size * 0.45.dp.toPx(),
                size * 0.65.dp.toPx(), size * 0.65.dp.toPx()
            )
            it.quadraticBezierTo(
                size * 0.45.dp.toPx(), size * 0.35.dp.toPx(),
                size * 0.30.dp.toPx(), size * 0.70.dp.toPx()
            )
            it.close()
            it
        }
        drawPath(path = mouthPath, color = TongueColor)
    }
}


